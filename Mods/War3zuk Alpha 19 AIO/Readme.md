War3zuk-Alpha-19.0 - AIO

Updated v4.5 to v4.6

Added GraceCornGlue To Campfire
Adjusted War3zuk Stoggy (craftTime)
Updated Camoes Biffana Icon

Fixed HD Cement Mixer Ambient Sound

Updated v4.4 to v4.5

Updated HD Pump Jack Code
Updated HD Pump Jack Material

Updated HD First Aid Kit Recipe
Updated resourceGlue Recipe
Updated resourceGlue Recipe
Updated HD High Torque Motor Recipe
Updated HD Cell Recipe

Added MagExtender Mod To
HD Double Barrel Snub
HD Spas 12 (All Versions)
HD MossBerg 500

Added StackSize 25k To
MedicalSplint
MedicalBandage
MedicalFirstAidBandage

Adjusted All HD Spas 12 Aim Down Sight


Launcher Edits:
Removed 0-CreaturePackHumans

Updated v4.3 to v4.4

Fixed HD Concrete Barrier Sell Price
Fixed HD Modular Wall Sell Price

Added HD War3zuk Moto (Motorbike)
Added HD War3zuk 4x4 (4x4 Truck)
Added HD War3zuk Buggy (Dune Buggy)

Added Localization For HD Power Relay

Added MuzzleBreak Mod To HD GlockG5 Auto
Removed Vanilla Silencer HD Mac10 Auto
Removed Vanilla Silencer HD Desert Eagle
Removed Vanilla Silencer HD Desert Eagle Punisher
Adjusted Texs Squirrel Bits Timer 30s
Fixed HD Impact Wrench Recipe (HDToolAndDieSet)

Animal LootTemplate Updated
Fixed PumpJack Localization Code
Updated HD Weapon Sounds (IceMachine.ogg)

Final Loot Rebalance Stable 19.0

Added HD Ice Machine Vanilla Model
Added HD Ice Machine Localization Code
Added HD Ice Machine 256x256 Icons

Removed Old Shaders From The Following Models
HD Cement Mixer
HD PicsBench
HD JukeBoxs
HD Mini Hatchet
HD Taza Hatchet
HD Paintings XXX
HD Penthrox Injection
HD Shuriken Katana
HD Wakizashi Katana
HD Dragon Katana

Updated The Following Models To Unity 2019.2
HD Impact Wrench


Launcher Edits:
Updated Bdubs Vehicle Overhaul
Updated Gamestage.xml
Updated 0-CreaturePackAnimals
Updated 0-CreaturePackZombies
Edited 0-CreaturePackAnimals
Edited 0-CreaturePackHumans
(Removed Ambient Sounds)
Removed 0-CreaturePackHumans
Will do a stripped down version at some point

Updated v4.2 to v4.3

Fixed Hd Taza Hatchet (Repair Smallstone)

Removed Old Shader From The Following Models
HD BedRoll
cntDumpster
HD Colt M1911A

Added RyuSen101's Ramen Noodles (Campfire + Cookingpot)
Removed Excluded Biome (Wasteland) Vanilla xml
Added All Vanilla Quests To Wasteland Biome

Added Localization For All HD Bench Addons

Launcher Edits:
Fixed War3zuk Military Camp Prefab
Updated 0-CreaturePackAnimals
Updated 0-CreaturePackHumans
Updated 0-CreaturePackZombies
Added OilShale To Deco Ores

Fixed 2 Prefabs Causing Restart Errors
xcostum_PeerCafe(by_Stallionsden)
xcostum_Stallionsdens_Ranch(by_Stallionsden)

Updated v4.1 to v4.2

Added medicalFirstAidBandageSchematic
Removed MedicalFirstAidKit

Added HD Silver Ingot (Sellable)
Added HD Gold Ingot (Sellable)
Added HD Diamond Ingot (Sellable)

Launcher Edits:
Added OfficialWar3zukAIOMap Custom Map (Edited)
Added War3zukWasteLandAIO (100% WasteLand)
Updated 0-CreaturePackAnimals
Updated 0-CreaturePackHumans
Updated 0-CreaturePackZombies

Updated v4.0 to v4.1

Fixed HD Scrap Wrench Recipe
Added HD Scrap Wrench Localization
Fixed HD Impact Wrench Schematic Scrap Recipe

Balanced Schematics Drops (Down .1)

Launcher Edits:
Removed xcostum_SuperStore(by_Volar) Bad FPS
Combined CompoPack 45 B180

Added Custom NitroMap (War3zukMPAIO) All Wasteland
Added Oil_Shale To WasteLand Decoration
Added Oil_Shale To WasteLand Viens
Removed WasteLand Cinderpiles
Removed Wasteland ScrapPiles

Updated v3.9 to v4.0

Fixed Distance BackPack Not Showing
Removed 4 Unused Icons

Launcher Edits:
Updated & Edited 0-CreaturePackAnimals
Updated & Edited 0-CreaturePackHumans
Updated & Edited 0-CreaturePackZombies
Updated & Edited War3zuk Creature Pack Patch

Updated v3.8 to v3.9

Fixed HD Juke Box ASevenfold Recipe
Updated Recipes For Custom Foods
Updated toolAnvil Code
Updated toolBellows Code

Added Tex's Squirrel Bits

Removed SmallTopAttachments From
HD Colt M1911
HD Colt M1911A
HD Desert Eagle
HD Desert Eagle Punisher
HD Spas 12 (All 5)


Launcher Edits:
Adjusted KheldonScreamerBears

Updated v3.7 to v3.8

Added RabidAries Cute Chick Pie (Campfire + Grill)

Fixed LockedLootContainerBoxCode
Fixed LockedVehicleLootCode

Fixed HD BR1NXlS Booper Xml
Fixed HD BR1NXlS Booper Recipe

Launcher Edits:
Removed Fluffy Bottoms pet Store (Bad FPS)
Removed xcostum_Multi_Trader_by_Sparrow (Bad FPS)
Updated Laotseu PaintJob (Big Update)

Updated v3.6 to v3.7

Following Items Are Sellable To Traders
HD Ruck Sack Upgrade
HD Armor Reinforced Padding
HD Armor Plastic Plating
HD Armor Military Plating
HD Armor Scrap Plating
HD Armor Iron Plating
HD Armor Steel Plating
HD Aero Dynamics Upgrade
HD Fuel Saver Upgrade
HD Head lights Upgrade
HD Turbo Upgrade
HD Night Vision Helmet Light

Added Grabbys Chilli Cheese Fries

Balanced Ethans Bam Hammer (Secondary Attack)
Balanced HD Xcalibur (Secondary Attack)

Added DrFuzzies Avenged Jukebox
Removed Vanilla Chainsaw Recipe (No Idea Why That Was There)

Launcher Edits:
Updated Apoc MultiTrader (Better FPS)
Updated xcostum_VarietiesDepartmentStore(by_Stallionsden)
Updated xcostum_PeerCafe(by_Stallionsden)
Updated xcostum_FarmerJoesNursery(by_Stallionsden)
Updated xcostum_LMART_Garage(byStallionsden)
Removed A Few Larger Laggy Prefabs
rwgmixer Edits, StallionsDen Prefabs Set To 2

Updated v3.5 to v3.6

HD Noob Block Upgrade Rate Defaulted At 2Mins

HD Night Vision Helmet Light Addon
Idea For The Night Vision Helmet Addon (Nikolay Lopatyuk)

Find The Trader Quest Paper (Crafted On Player)
This WILL Reset Your Player But Old Saves Will Work

Quests Buried Supplies Tier 1 To 6 B180 (This Will Reset Player Stats)

Launcher Edits:
Edited Clown
Nauti-Baskin Animal Sanctuary Fixed

Updated v3.4 to v3.5

Readded Spas 12 Sounds
Readded MossBerg 500 Sounds

Find The Trader Quest Paper (Crafted On Player)
Commented Out On Purpose As It Will Reset Characters
Will Enable On The Next Exp Update From The Fun Pimps

Rebuild Trader Code
Opening Times 5:59am
Closing Times 11:59pm
Reset Interval 1 Day
All Traders 1 To 8

Rebalanced The Following Groups B178
War3zukHDWeaponParts
War3zukHDWeapons
War3zukHDBenches
War3zukHDBenchAddons
War3zukHDTools
War3zukHDMilitary
War3zukHDFlares
War3zukHDCarePackages
War3zukHDFoods


Launcher Edits:
Added Bdubs Vehicle OverHaul
Kheldon Screamer Bears Adjusted

Updated v3.3 to v3.4

Rebuilt HD Knife Code
Fixed GlockG5 Modded Ammo

Updated Recipes On The Following
Congealed Metal
Steel Polish
Titanium Polish
HD Knife

Removed M60_Fire From HD Karabiner 98k

Added Shaved Ice
Added Pikalyns Patbingsu (Made On Character)

Updated Recipe For The Following
HD GunBench
HD WorkBench
HD PicsBench
HD AmmoBench

Launcher Edits:
Removed <property name="LookAtAngle" value="0"/>

Updated v3.2 to v3.3

Menu Button Alignment Complete B177

Changed Vanilla medicalBandage (Apply Anytime)
Changed Vanilla medicalSplint (Apply Anytime)
Changed Vanilla medicalPlasterCast (Apply Anytime)

Updated Recipes As Follows
HD Rocket Launcher
HDRocketLauncherNukeRound
Warm Chunky Meat Stew
Warm Chunky Sham Chowder
Warm Chunky Hobo Stew
Warm Chunky Steak And Potato
HDScrapIronAxe
HDScrapIronPickAxe
HDScrapIronShovel
HDScrapIronClub
HDScrapIronShiv
HDCncHandGuns
HDCncAutomatics
HDCncRifles

Launcher Edits:
Removed ALL FireArms Bandits IE Guns
Xyth Rebalanced 0-CreaturePackZombies
Xyth Rebalanced 0-CreaturePackHumans
Xyth Rebalanced 0-CreaturePackAnimals

Updated v3.1 to v3.2

Rebuilt Backpack XUI Code B177

Added BunnyNZs Bacon And Egg Pie (Campfire + Pot)
Added Curmudgeons Beef Chili Pie (Campfire + Pot)

Adjusted HD Desert Eagle Handling
Adjusted HD Desert Eagle Punisher Handling

Rebuilt Shock Ammo Code
Rebuilt Inc Ammo Code

Rebuilt HD Mac10 Auto Code
Rebuilt HD M3 Auto Code + Aim Down Sight
Rebuilt HD HK33 Hybrid Code
Rebuilt HD AK Hybrid ACP Code

Rebuilt Cook Quest Class B177
Edited Farmer Class (Magnum)
Edited Medic Class (DesertVulture)

Launcher Edits:
Added WidowSpider 01
Added WidowSpider 02
Added WidowSpider 03

Removed SMXmenu (Till Updated)
Removed ZMXmenuWAIOCP20 Patch (Till Updated)

Updated v3.0 to v3.1

Removed War3zuk 4x4 (Now PaintJob Modlet)

Rebalanced Loot Tables B173
Edited HD Truck Elevator x5 Act Distance
Edited HD Truck Elevator x10 Act Distance
Added HD Truck Elevator x5 Sound
Added HD Truck Elevator x10 Sound

Added radiatorHouse01 To Regrow (360)

Rebuilt Building Supply Flare Code (Rebalanced Loot)
Rebuilt Health Supply Flare Code (Rebalanced Loot)
Rebuilt Ammo Supply Flare Code (Rebalanced Loot)
Rebuilt Food Supply Flare Code (Rebalanced Loot)

Adjuted HD Electrolyte Recipe (Craft On Player)
Fixed Titanium Polish Localization

Updated Texture HD Gas Generator
Updated Texture HD Generator

Launcher Edits:
Sly Ship Spawns Balanced
Animal Sanctuary Fixed Spawns Balanced
Removed 15 Prefabs (Bad FPS)
Rebalanced Supply Crate Loot
0-CreaturePackAnimals Updated
0-CreaturePackHumans Updated
0-CreaturePackZombies Updated
Added PaintJob Modlet By Eric

Updated v2.9 to v3.0

Added War3zuk 4x4 (By Éric Beaudon)

Updated HD Spas 12 Model (Paid)
Updated HD MossBerg 500 (Paid)
Updated HD Class Builder (Paid)

Fixed HD Vulcan 9mm
Updated HD Vulcan 7.62mm
Updated HD Vulcan 50mm

Launcher Edits:
RWGmixer File Finished
Removed Xvanilla For Less Duping
Edited Sleepers On SlyShip
Edited Sleepers On Animal Sanctuary
Edited Flashing Lights From Rave Factory Prefab

Updated v2.8 to v2.9

Rebuilt (itemInfoPanel UI)

Re-Balanced Animal Spawns
Re-Balanced Phantasm Spawns
Re-Balanced Demo Spawns
Re-Balanced Vulture Spawns

Added HD BlunderBuss PBR Model (Paid)
Added HD BlunderBuss Mag Extender
Removed Old Addons
Adjusted Light Mod Position

Launcher Edits:
Re-Balanced Spider Spawns
Added Custom Alpha 18.4 To Alpha 19 Hybrid RWGMixer File
Updated CompoPack 44 From Magoli

Updated v2.7 to v2.8

Updated ToolAnvil

Added Naoyans Yakitori (Campfire + Grill)
Updated Buried Supplies Quests Balanced

Rebuilt Cook Quest Tier 1 To 5
Tier 1 1x 5 Custom Food
Tier 2 2x 5 Custom Food
Tier 3 3x 5 Custom Food
Tier 4 4x 5 Custom Food
Tier 5 5x 5 Custom Food

Rebuild Lovers Pizza (1 Recipe Now)
Rebuilt Donnas Fubar Stew (1 Recipe Now)

Fixed Localization For The Following
Ipossoms Pavlova
MsTeresas Burger Patty

Fixed UI Window Issue From B163

Launcher Edits:
Xyth Fixed GuppyInsect
Added Murder Hornet Insect
Added Mods Section To Bdubs Vehicles
Creature Packs Updated

Updated v2.6 to v2.7

ZombieLootDropBag PBR Model (Final)
This Is What It Will Be From Now On
Added 2m Tall Craftable Version

Rebuilt HD Tactical AR PBR Model (Paid)
Added HD Tactical AR Silencer PBR Model (Paid)
Added HD Tactical AR Reflex PBR Model (Paid)
Added HD Tactical AR Scope PBR Model (Paid)
Added Icons 256x256

Re-Balanced Warm Chunky Foods Stam,Food,Health

Fixed CntBedroll Placement

Launcher Edits:
CreaturePack Humans Edits
Balanced NPC Bandits
Balanced NPC Bandits Block Dmg

Switched From GitHub To GitLab (Includes)

Official World (War3zukAIOAlpha19)
Full CompoPack Alpha 19 Build 44 Edited

Updated v2.5 to v2.6

HD War3zuk Dune Buggy XML Code Balanced

Khaine's PreLoad Code
HD WoodBars Upgrade DownGrade Path Fixed

Rebuilt HD Auger Code
Rebuilt HD Chainsaw Code

Fixed HD Desert Eagle Not Seeing Its Scope
Fixed HD Desert Eagle Punisher Not Seeing Its Scope
Fixed HD Dragunov SVD Not Seeing Its Scope

Fixed CntCoffin Code
Added 2 Coffin Variants
Added Modified Compo Pack

Launcher Edits:
NPC Bandits All Short Range
NPC Humans Mid Ranged
Added Modified Compo Pack

Updated v2.4 to v2.5

Added HD War3zuk Dune Buggy PBR Model (Paid)
Added Icons 256x256
Added NavUI Icons 256x256
Unlocked Via GreaseMonkey L4

Fixed HD Shotguns Bad RNG Code
Fixed HD Scrap Tools Code
Fixed Engineering Class Crossbow
Added 256x256 Icon For Crash Bandicoot Statue

Edited Zombie AI (Improved Sight Threshold Hopefully)
Edited CntHospitalBed (Sound)

Rebuilt Spawn Groups Animals
Rebuild Spawn Groups Zombies

Launcher Edits:
Added The Following Prefabs To Quests,Fetch,Clear L1
Nauti Burrows (Harry Potter Film)
Nauti Lattez Club
Nauti Pentagon Building
Nauti Baskin Animal Sanctuary
War3zuk Housing Block
War3zuk JailHouse
War3zuk Bike Repair Shop
War3zuk Multi Storey Plazza

Adjusted SlyShip Sleeper Spawning

Added 0-CreaturePackAnimals (Xyth Community Project)
Added 0-CreaturePackZombies (Xyth Community Project)
Added 0-CreaturePackHumans (Xyth Community Project)

Edited Zombie AI (Improved Sight Threshold Hopefully)
NPC Bandits No Longer Attack Zombies

Updated v2.3 to v2.4

Rebuilt Spawning Code
Rebuilt Giant Animals
Rebuilt Demos
Rebuilt Phantasm's

Fixed Spawning Code
Fixed Entity Code

Adjusted Entity Group Spawns B169

Launcher Edits:
Readded Full Patched SMX UI System
Update 2 Prefabs (B169 Changes)

Updated v2.2 to v2.3

Added SuperCorn Glue
Adjusted HD Forge Particle
Adjusted HD Working Fire particle

Added Vanilla Bonus To Bundle Crafting

Added Entity ZombiePhantom
Added Entity ZombiePhantomFire
Added Entity ZombiePhantomRAD

Removed Minion Lootbag
Added Crash Bandicoot Custom PBR Model (Paid)

Removed CandyTin From Recipes B169 Changes
Rebuilt HD Breaching C4
Rebuild HD Land Mine

ADDED CUT OFF LINE TO THE entityclasses.XML
Follow Its Instructions If You Wish To Remove
The Custom RuckSacks For Player & Loot

Updated v2.1 to v2.2

Fixed Bench UI Interactions

Rebuilt HD Gas Generator Code
Rebuilt HD Generator Code
Adjusted HD Desert Eagle Punisher Cross Hair

Rebuilt Fubar Steak Code
Rebuilt Donnas Fubar Stew Code
Rebuilt Baby Chick Code
Rebuilt Chicken Nuggets Code
Rebuilt KillJoyJessicas HotWings Code

CokaGamings Flaming Hot Cheetos (Campfire + Cookinpot)

Rebuilt HD Colt M1911 Code
Rebuilt HD Colt M1911A Code

Removed x2 x4 Scopes From HD Arctic Warfare
Fixed HD Arctic Warfare Scope
Removed x2 x4 Scopes From HD Cheytac M300
Fixed HD Cheytac M300 Scope

Rebuilt HD MossBerg 500 PBR Model (Paid)
Rebuilt HD MossBerg Silencer PBR Model (Paid)
Added HD MossBerg Reflex PBR Model (Paid)
Adjusted All Sights

Rebuilt HD Dragunov SVD Animation

Updated v2.0 to v2.1

HD Spas 12 Addons Fixed
Fixed Spas Aim Down Sights
HD Mossberg 500 Addons Fixed
Fixed HD Mossberg 500 Aim Down Sights

Added QueenOfScreamz8s Chili Burrito (CampFire + Grill)
Added Papas Bare Back Ribs  (CampFire + Grill)

Replaced Old free Xcalibur Model
Added HD Xcalibur PBR Model (Paid)
Added HD Xcalibur HD Icon

Updated Ethans Bam Hammer Addon Position
Updated Iron Axe Addon Position

Update XUI Code

Launcher Edits:
Fixed UI Issue
Removed No Longer Used Scrap
Edited UI Patch Health Bar

Updated v1.9 to v2.0

Adjusted HD RuckSack Upgrade Loot Chance
Rebuilt HD Breaching C4 Code

Following Items Can Now Upgrade To Titanium
HD Mini Hatchet
HD Tazas Hatchet

Updated v1.8 to v1.9

Fixed CntGasPump Model
Rebalanced Loot (According To B163 Update)
Fixed Loot Probability Template

Fixed Buried Supplies LootGroup
Added Custom Sound Desert Vulture
Fixed Care Packages Not Stacking
Fixed Military Tablets Not Stacking

Added HD RuckSack Upgrades To
Army Trucks
Dumpsters
Lockers

Launcher Edits:
Eddied Sparrow Multi Trader (30fps Extra)

Updated v1.7 to v1.8

Updated 4 War3zuk Custom Prefabs
Added Diggitys Chilli Pork Chops (CampFire + Grill)
Added klausgamer14s Beef steak And Fries (CampFire + Grill)
Rebuilt HD Mini Hatchet Code
Rebuilt Tazas Hatchet Code

Added HD Double Barrel Snub PBR Model (Paid)

Fixed LootGroup (B163 Changes)
Fixed Trader LootGroups (B163 Changes)
Added New Changes To All VM Venders (B163 Changes)
Added New Loot Additions To All VM Venders (B163 Changes)

Launcher Edits:
Removed Snufkins Zombies (Needs More Testing)
Fixed Bdubs Vehicles Modlet
Fixed CreaturePack Modlet
Fixed CreaturePack entries So No Warnings

Updated v1.6 to v1.7

75 Models Updated To Unity 2019.2
Updated Explosion Indexes
Re-Balanced Pathogen Infection %

Updated v1.5 to v1.6

Fixed Dropped BackPack Size
Reduced Steel For HD Wrench
Adjusted HD Ruck Sack Upgrade
Added Male Urinal To LootGroup
Adjusted Urinal And Toilet Loot

Rebuilt HD Hybrid HK33 Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD AK Hybrid ACP Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD M4A1 Carbine Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD Double Barrel Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD Spas 12 (All 5) Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD Mossberg 500 Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD Winchester 1866 Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD Karabiner 98k Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD Dragunov SVD Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD Arctic Warfare Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD Cheytac M300 Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD Rocket Launcher Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD Flame Thrower Unity 2019.2
Added Linear Lighting To Model

Updated v1.4 to v1.5

Rebuilt HD GlockG5 18 Auto Unity 2019.2
Rebuilt HD GlockG5 18 Auto Reflex Sight
Rebuilt Animations
Added Linear Lighting To Model

Rebuilt HD Desert Eagle Punisher Unity 2019.2
Rebuilt Animations
Added Linear Lighting To Model

Rebuilt HD Desert Eagle Unity 2019.2
Rebuilt Animations
Added Linear Lighting To Model

Rebuilt HD BlunderBuss Unity 2019.2
Added Linear Lighting To Model

Rebuilt HD Mac10 Auto Unity 2019.2
Rebuilt Animations
Added Linear Lighting To Model

Rebuilt HD M3 Auto Unity 2019.2
Rebuilt Animations
Added Linear Lighting To Model

Updated Colider HD Forge
Fixed Farmer Class Schematic Error

Updated v1.3 to v1.4

Removed Doubling Of Health To
Bear,Dog,Grace

NO More Edits To The Quest XML Unless Its A Bug
Removed The Vanilla Buried Supplies Quest 1 & 2
Rebuilt Buried Supplies Quests Tier 1 To 6

Removed Find The Trader Quest
Reduced Food Consumption In Vehicles

Rebuilt HD Forge Unity 2019.2
Rebuilt HD Working Fire Unity 2019.2

Rebuilt HD Colt M1911 Unity 2019.2
Rebuilt Animations / Reflex Sight
Added Linear Lighting To Model

Rebuilt HD Colt M1911A Unity 2019.2
Rebuilt Animations / Reflex Sight
Added Linear Lighting To Model

Updated v1.2 to v1.3

Enabled Health Bar (Non God Mode)

Fixed cntHospitalBed Model (Updated Sound)
Fixed Loot Tables
Fixed Farmer Starter Class Wrong Weapon

Rebuilt HD Desert Eagle Code 115/120 Dmg
Rebuilt HD Desert Eagle Punisher Code 115/120 Dmg

Rebuilt HD Arctic Warfare Code
Repositioned Zoom
Damage Needed Balancing

Rebuilt HD CheyTac M300 Code
Repositioned Zoom, Barrel
Damage Needed Balancing

Rebuilt HD Dragunov SVD Code
Rebalanced Damage

Rebuilt HD Winchester 1866 Code
Doubled Mag Capacity x2 Damage
Fire rate x2

Removed Onscreen Nav Icons

Updated v1.1 to v1.2

**************************
**Requires Fresh Game**
**May Reset Player Stats**
**************************
Added Cook Class 5 Quests
Added HD Cook VM (Vender)
**************************************************************
Added Reward: Non Craftable HD Cooks Cleaver PBR Model (Paid)
Added Reward: Non Craftable HD Cooks Hammer PBR Model (Paid)
**************************************************************

Fixed Warm Chunky Mash And Meat Name
Added Warm Chunky Shepards Pie
Warm Chunky Gumbo Stew

Rebuilt All Custom Player Foods
Rebuilt All Warm And Chunky Foods

Rebuilt HD Double Barrel Code
Rebuilt HD Spas 12 Code (All 5)
Rebuilt HD Vulcan 9mm Code
Rebuilt HD Vulcan 7.62mm Code
Rebuilt HD Vulcan 50mm Code
Rebuilt HD Colt M1911 Code
Rebuilt HD Colt M1911A Code

Rebuilt HD M4A1 Carbine Code
Rebuilt HD Tactical AR Code

Rebuilt All Shock Ammo Code
Rebuilt All Inciniary Ammo Code

Replaced The STUPID Vanilla Steel Axe

Updated v1.0 to v1.1

Rebuilt HD Scrap Iron Axe Code
Rebuilt HD Scrap Iron PickAxe Code
Rebuilt HD Scrap Iron Shovel Code
Rebuilt HD Scrap Iron Club Code
Rebuilt HD Scrap Iron Shiv Code
Added HD Scrap Iron Wrench PBR Model (Paid)

Rebuilt HD Weapons Code (Balanced Dmg)
Fixed Player Storage Size (9x10) Maxed

Rebuilt HD Wrench Code
Rebuilt HD Impact Driver Code

Rebuilt HD Junk Turret 9mm
Rebuilt HD Junk Turret 7.62
Rebuilt HD Junk Turret 50mm
Rebalanced Their Damage Approx x2 Higher
Extended Turret Range By 300%

Rebuilt HD Xcalibur
Rebuilt HD Auger
Rebuilt HD Chainsaw

Updated HD Generator (Textured)
Updated HD Gas Generator (Textured)

Fixed HD Colt M1911 Code
Fixed HD Colt M1911A Code

Replaced Both Vanilla Dumpsters PBR Model (Paid)
Added 32 Custom Prefabs (Vanilla Lod Errors)

Updated v0.0 to v1.0

Fixed & Updated Inventory Backpack 105 Slots
Fixed & Updated 10x Crafting Slots

Rebuilt Class Quests Completely
Rebuilt War3zuk Quest 01
Rebuilt War3zuk Quest 02
Removed Tier1To6 Burried Quests (Will Rebuild At Some Point)
Rebuilt Pathogen Code All 4
Rebuilt HD First Aid Kit Code

Rebuilt HD Military Helmet Code
Rebuilt HD Military Vest Code
Rebuilt HD Military Gloves Code
Rebuilt HD Military Legs Code
Rebuilt HD Military Boots Code

Rebuilt Grand Spartans Sword Code
Rebuilt Mrs Spartans Sword Code
Rebuilt HD Machete Code
Rebuilt HD Wakizashi Katana Code
Rebuilt HD Shuriken Katana Code
Rebuilt HD Dragon Katana Code
Rebuilt Ethans Bam Hammer Code

Rebuilt Br1nxs Booper PBR Model (Paid)
Rebuilt Br1nxs Booper Code

Rebuilt HD Colt M1911 PBR Model (Paid)
Rebuilt HD Colt M1911A PBR Model (Paid)
Rebuilt HD GlockG5 18 Auto PBR Model (Paid)
Rebuilt HD Desert Eagle PBR Model (Paid)
Rebuilt HD Desert Eagle Punisher PBR Model (Paid)
Added Desert Eagle Scope PBR Model (Paid)
Added Desert Eagle Punisher Scope PBR Model (Paid)

Added Player Dropped Ruck Sack PBR Model (Paid)
Updated Ethans Bam Hammer (Position,Scale & Reflections)
Stripped Out All Modified Zombies (UMA)
Added Warm Chunky Mash And Meat (CampFire + Cookingpot)
Updated HD Penthrox Injection Model
Updated HD Floor Safe Texture
Updated HD Fuel Storage Texture
Updated HD Wrench Texture
Updated HD AmmoBench Texture
Updated HD Cement Mixer Texture
Updated HD GunBench Texture
Updated HD PicsBench Texture
Updated HD Range Cooker Texture
Updated HD Water Dispenser Texture
Updated HD WorkBench Texture
Updated HD Working Fridge Texture

HD Bollock Rot Injection (Uses Injector PBR Model Paid)
HD Bird Flu Injection (Uses Injector PBR Model Paid)
HD Brain Herpes Injection (Uses Injector PBR Model Paid)
HD MRSA Injection (Uses Injector PBR Model Paid)

Fixed HospitalBed Colider
All 9 BodyBags Updated And Resized
Bedroll Model Resized

Rebuilt HD Katanas & Retextured
Replaced Vanilla Munitions Box PBR Model (Paid)
Replaced Vanilla CntCoffin PBR Model (Paid)

Converted All 24 POIS To Alpha 19 Mine + Nautis
Converted 8 Old Prefabs & Updated To ALpha 19

Added HD Modular Wall (Cant Be Stacked)
Fixed HardCoded Duping Bug (Thanks For Pointing It Out (P0yzin)
Fixed Inventory Bug (Thanks Sirillion)
Fixed Medical Injector Postioning
Added All Giants To Mod

Fixed Hand Items
(Vulture Gives BUFFBirdFlu)
(Stripper Gives BUFFBollockRot)
(Nurse Gives BUFFMRSA)
(Hazmat Gives BUFFBrainHerpes)